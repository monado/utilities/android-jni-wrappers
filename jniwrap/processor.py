# Copyright 2020-2021, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
"""Contains the processor to extract data from javap."""

import logging
import platform
import subprocess
from io import StringIO
from typing import List

from typing_extensions import Final

from .class_data import ClassData
from .file_process import LinewiseFileProcessor
from .members import Constructor, construct_member


class _ClassExtractor(LinewiseFileProcessor):
    def __init__(self, classdata: ClassData) -> None:
        """Constructor."""
        super().__init__()
        self._classdata: Final[ClassData] = classdata

    def _process_named(self, decl: str, java_prototype: str) -> None:
        declaration_parts = decl.strip().split()

        # drop access modifier
        declaration_parts = declaration_parts[1:]

        is_static = False

        while declaration_parts and declaration_parts[0] in (
            "abstract",
            "static",
            "final",
            "native",
            "default",
        ):
            if declaration_parts[0] == "static":
                is_static = True

            declaration_parts = declaration_parts[1:]

        signature = self.next_line.strip().replace("descriptor: ", "")
        if len(declaration_parts) == 1:
            member = Constructor(signature, java_prototype, name=declaration_parts[0])
        else:

            name = declaration_parts[1]
            if name == "void":
                raise RuntimeError(
                    "Think we forgot to drop some keyword: " + str(declaration_parts)
                )
            member = construct_member(name, signature, java_prototype, is_static)

        if member:
            self._classdata.members.append(member)

    def process_line(self, _line_num: int, line: str) -> None:
        """Process a line supplied by LinewiseFileProcessor."""
        # print(line_num, line)
        if "public" not in line:
            return

        if "class" in line:
            return

        if "interface" in line:
            return

        if "(" in line:
            parts = line.strip().split("(")
            self._process_named(parts[0], line.strip())
            return

        self._process_named(self.line_rstripped.rstrip(";"), line.strip())


class SigExtractor:
    """Class to extract useful data from javap."""

    def __init__(self, classpath: List[str]) -> None:
        """Constructor."""
        super().__init__()
        self._log = logging.getLogger(self.__module__)
        delim = ";" if platform.system() == "Windows" else ":"
        self._classpath_param = delim.join(classpath)

    def extract_class(self, package: str, classname: str) -> ClassData:
        """Run javap and extract data on a class type."""
        clazz = f"{package}.{classname}"
        self._log.info("Extracting data on class type %s", clazz)
        cmd = ["javap", "-s", "-public", "-classpath", self._classpath_param, clazz]
        self._log.debug("Running javap: %s", str(cmd))
        proc = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8")

        self._log.debug("output:\n%s", proc.stdout)

        # Now, parse the output to populate this class's data
        classdata = ClassData(package, classname)
        _ClassExtractor(classdata).process_file(
            filename="stdout", file_handle=StringIO(proc.stdout)
        )
        classdata.assign_decorations()
        return classdata

    def mock_class(self, package: str, classname: str) -> ClassData:
        """Act like extract_class but don't actually run javap."""
        clazz = f"{package}.{classname}"
        self._log.info("Generating mock object for class type %s", clazz)
        classdata = ClassData(package, classname)
        classdata.assign_decorations()
        return classdata
