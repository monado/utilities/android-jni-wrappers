# Copyright 2020-2021, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
"""Provides the object type used to track a wrapped class."""

import logging
from typing import Dict, Iterator, List, Set, Tuple

from .members import Member, construct_member
from .types import (
    JObject,
    MemberDict,
    NamedMemberOptions,
    OptionalContext,
    OptionsDict,
    VisitorBase,
    qualified_name_parts_to_qualified_cpp,
)

_logger = logging.Logger(__name__)


class _DecorationTracker:
    """Assign decorations to duplicate member names.

    The first of a given name gets no decoration, but the second, third, etc. gets 1, 2, 3...
    """

    def __init__(self) -> None:
        self.decorations: Dict[str, int] = {}
        """Maps name to next decoration value to assign."""

    def assign_next(self, name: str) -> str:
        """Return the name suffix for a new item named ``name``, and update internal state."""
        decoration = self.decorations.get(name)
        if decoration is None:
            decoration = ""
            decoration_num = 1
        else:
            decoration_num = decoration + 1
        self.decorations[name] = decoration_num
        return str(decoration)


# pylint: disable=too-many-instance-attributes
class ClassData:
    """Data for a single Java class."""

    def __init__(self, package: str, classname: str) -> None:
        """Construct empty class data from a dotted qualified package and class name."""
        self.type = JObject(package.split("."), classname)
        self.members: List[Member] = []
        self.meta_droppable: bool = True
        self.meta_defer_drop: bool = False
        self._decorations_assigned: bool = False
        self.base: str = "ObjectWrapperBase"
        self.base_include: str = "ObjectWrapperBase.h"
        self.base_parts: List[str] = [self.base]
        self.static_init: bool = False
        self._sort_members: bool = False
        self.provide_qualified_name: bool = False

    def apply_visitor(self, visitor: VisitorBase) -> None:
        """Visit children recursively."""
        visitor.visit_class(self)
        for member in self.members:
            member.apply_visitor(visitor)

    def set_base(self, dotted_qualified_name: str) -> None:
        """Set the base class you wish for this type."""
        base = JObject.from_qualified_type_name(dotted_qualified_name)
        self.base_parts = base.parts
        self.base = base.cpp_classname
        self.base_include = f"{base.dotted_package_name}.h"

    def apply_options(self, options: OptionsDict) -> None:
        """Apply a dictionary of options."""
        if not options:
            return

        base = options.get("base")
        if base is not None:
            self.set_base(base)

        droppable = options.get("droppable")
        if droppable is False:
            self.meta_droppable = False

        provide_qualified_name = options.get("provideQualifiedName")
        if provide_qualified_name is not None:
            self.provide_qualified_name = provide_qualified_name

        defer_drop = options.get("deferDrop")
        if defer_drop is not None:
            self.meta_defer_drop = defer_drop

        static_init = options.get("staticInit")
        if static_init is not None:
            self.static_init = static_init
            if self.static_init:
                # implies not droppable
                self.meta_droppable = False

        sort_members = options.get("sortMembers")
        if sort_members is not None:
            self._sort_members = sort_members

        if "extraMembers" in options:
            for member_data in options["extraMembers"]:
                member_data: MemberDict
                new_member = construct_member(
                    member_data["name"],
                    member_data["jniSignature"],
                    is_static=member_data["static"],
                    java_prototype=member_data.get("javaPrototype"),
                )

                self.members.append(new_member)

    def assign_decorations(self) -> None:
        """Assign decorations to any overloaded member names."""
        if not self._decorations_assigned:
            self._decorations_assigned = True
            decorations = _DecorationTracker()
            for member in self.members:
                decoration = decorations.assign_next(member.name)
                member.set_decoration(decoration)

    @property
    def package_include(self) -> str:
        """Get include for this package's wrapper."""
        return '"{}.h"'.format(".".join(self.type.package))

    def get_declaration_required_includes(
        self, context: OptionalContext = None
    ) -> Set[str]:
        """
        Get a set of all includes required for the declaration.

        This recursively gets info from each member and all associated classes.

        Includes are double-quoted or bracketed as appropriate.
        """
        includes = set()
        includes.add(f'"{self.base_include}"')
        for member in self.members:
            includes.update(member.get_declaration_required_includes(context))
        includes.discard(self.package_include)

        return includes

    def get_definition_required_includes(
        self, context: OptionalContext = None
    ) -> Set[str]:
        """
        Get a set of all includes required for the definition.

        This recursively gets info from each member and all associated classes.

        Includes are double-quoted or bracketed as appropriate.
        """
        includes = set()
        for member in self.members:
            includes.update(member.get_definition_required_includes(context))
        includes.discard(self.package_include)

        return includes

    def get_forward_declarations(self) -> Set[Tuple[str, ...]]:
        """
        Get a set of all required forward declarations.

        Each element is a tuple of namespaces.
        """
        fwd_decls = set()
        for member in self.members:
            fwd_decls.update(member.get_forward_declarations())
        return fwd_decls

    @property
    def meta_base_name(self) -> str:
        """Get the meta class base name."""
        return "MetaBaseDroppable" if self.meta_droppable else "MetaBase"

    def get_meta_base_initializer(self) -> str:
        """Get the initializer for this class's Meta structure's base type."""
        args = [f"{self.type.cpp_classname}::getTypeName()"]
        if self.static_init:
            args.append("clazz")

        return f'{self.meta_base_name}({", ".join(args)})'

    def get_meta_constructor_body(self) -> str:
        """Get the body for this class's Meta structure's constructor."""
        if self.meta_defer_drop:
            return "if (!deferDrop) { MetaBaseDroppable::dropClassRef(); }"
        if self.meta_droppable:
            return "MetaBaseDroppable::dropClassRef();"
        return ""

    def process_member_options(
        self, member_options: Dict[str, NamedMemberOptions]
    ) -> None:
        """Keep only the members whose name are in the provided list, and store the member options."""
        keepers = set(member_options.keys())
        old_members = self.members[:]
        _logger.debug("keepers: %s", str(keepers))
        _logger.debug("old members: %s", str([x.name for x in old_members]))
        self.members = [x for x in old_members if x.name in keepers]
        kept_names = {x.name for x in self.members}
        _logger.debug("kept members: %s", str(kept_names))
        missing = keepers - kept_names

        if missing:
            _logger.warning(
                "WARNING: Class %s does not have the following requested members: %s",
                self.type.qualified_name,
                str(missing),
            )
        else:
            assert len(keepers) == len(kept_names)

        # Tell each member about its options
        for member in self.members:
            if member.name in member_options:
                member.assign_member_options(member_options[member.name])

        # Finally, remove any overloads marked to skip
        self.members = [x for x in self.members if not x.should_skip_wrapping]

    def generate_meta_constructor_args(self) -> Iterator[Tuple[str, str, str]]:
        """
        Get the arguments for the meta class.

        Yields (type, name, default_value) tuples.
        """
        if self.static_init:
            yield ("jni::jclass", "clazz", "= nullptr")
        if self.meta_defer_drop:
            yield ("bool", "deferDrop", "= false")

    @property
    def sorted_members(self):
        """Get the members sorted by name, for reproducibility, if requested."""
        if self._sort_members:
            return sorted(self.members, key=lambda x: x.name)
        return self.members

    def get_decl(self, context: OptionalContext = None) -> str:
        """Get the header-file part of this class's wrapper."""
        self.assign_decorations()
        members = [x.get_wrapper_decl(context) for x in self.sorted_members]
        meta_decls = [x.get_meta_decl(context) for x in self.sorted_members]
        minimally_qualified_base = qualified_name_parts_to_qualified_cpp(
            self.base_parts, context
        )
        lines = [
            "/*!",
            f" * Wrapper for {self.type.qualified_name} objects.",
            " */",
            f"class {self.type.cpp_classname}: public {minimally_qualified_base} {{",
            "public:",
            f"using {self.base}::{self.base};",
            "static constexpr const char *getTypeName() noexcept {",
            f'    return "{self.type.slashed_name}";',
            "}",
            "",
        ]

        if self.provide_qualified_name:
            lines.extend(
                [
                    "static constexpr const char *getFullyQualifiedTypeName() noexcept {",
                    f'    return "{self.type.qualified_name}";',
                    "}",
                    "",
                ]
            )

        lines.extend(["\n\n".join(members), ""])

        if self.static_init:
            lines.extend(
                [
                    "/*!",
                    " * Initialize the static metadata of this wrapper with a known",
                    " * (non-null) Java class.",
                    " */",
                    "static void",
                    "staticInitClass(jni::jclass clazz)",
                    "{",
                    f'    Meta::data(clazz{", true" if self.meta_defer_drop else ""});',
                    "}",
                    "",
                ]
            )
        arg_decls_with_defaults = [
            " ".join(x) for x in self.generate_meta_constructor_args()
        ]
        arg_decls = [" ".join(x[:2]) for x in self.generate_meta_constructor_args()]
        args = [x[1] for x in self.generate_meta_constructor_args()]
        lines.extend(
            [
                "/*!",
                " * Class metadata",
                " */",
                f"struct Meta : public {self.meta_base_name} {{",
                "\n".join(meta_decls),
                "",
                "  /*!",
                "   * Singleton accessor",
                "   */",
                f'  static Meta &data({", ".join(arg_decls_with_defaults)}) {{',
                f'      static Meta instance{{ {", ".join(args)} }};',
                "      return instance;",
                "  }",
                "",
                "private:",
                f'  {"explicit " if arg_decls else ""} Meta({", ".join(arg_decls)});',
                "};",
                "};",
                "",
            ]
        )
        return "\n".join(lines)

    def get_inline_implementations(self, context: OptionalContext = None) -> str:
        """Get the impl-header-file part of this class's wrapper."""
        self.assign_decorations()

        def make_member(x):
            if self.meta_defer_drop and x.is_static:
                # Use our "deferDrop" ability on static members.
                return x.get_wrapper_defn(
                    self.type.cpp_classname,
                    context,
                    meta_instance="data",
                    before="auto &data = Meta::data(true);",
                    after="data.dropClassRef();",
                )
            return x.get_wrapper_defn(self.type.cpp_classname, context)

        members = [make_member(x) for x in self.sorted_members]
        return "\n\n".join(members) + "\n"

    def get_meta_constructor(self) -> str:
        """Get the full constructor of this class's Meta struct, usually the only thing in the cpp file."""
        self.assign_decorations()
        initializers = [self.get_meta_base_initializer()]
        initializers.extend(m.get_meta_initializer() for m in self.sorted_members)
        # for this purpose we drop the default value
        arg_decls = (" ".join(x[:2]) for x in self.generate_meta_constructor_args())
        lines = [
            f'{self.type.cpp_classname}::Meta::Meta({", ".join(arg_decls)}) : ',
            ",\n".join(initializers),
            "{",
            self.get_meta_constructor_body(),
            "}",
        ]
        return "\n".join(lines)
