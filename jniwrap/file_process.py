#!/usr/bin/python3
#
# Copyright (c) 2018-2021 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Author(s):    Rylie Pavlik <rylie.pavlik@collabora.com>
"""Utilities for processing files."""

from pathlib import Path
from typing import List, Optional, TextIO, Union


class LinewiseFileProcessor:
    """A base class for code that processes an input file (or file handle) one line at a time."""

    def __init__(self) -> None:
        """Constructor."""
        self._lines: List[str] = []
        self._line_num: int = 0
        self._next_line: Optional[str] = None
        self._line: str = ""
        self._filename: Path = Path()

    @property
    def filename(self):
        """Get the Path object of the currently processed file."""
        return self._filename

    @property
    def relative_filename(self):
        """Get the current file's Path relative to the current working directory."""
        return self.filename.relative_to(Path(".").resolve())

    @property
    def line(self) -> str:
        """Get the current line, including any trailing whitespace and the line ending."""
        return self._line

    @property
    def line_number(self):
        """Get 1-indexed line number."""
        return self._line_num

    @property
    def line_rstripped(self) -> Optional[str]:
        """Get the current line without any trailing whitespace."""
        if self.line is None:
            return None
        return self.line.rstrip()

    @property
    def trailing_whitespace(self):
        """Get the trailing whitespace of the current line.

        Specifically, what gets removed when accessing line_rstripped.
        """
        stripped = self.line_rstripped
        if stripped is None:
            return None
        non_whitespace_length = len(stripped)
        return self.line[non_whitespace_length:]

    @property
    def next_line(self) -> Optional[str]:
        """Peek at the next line, if any."""
        return self._next_line

    @property
    def next_line_rstripped(self):
        """Peek at the next line, if any, without any trailing whitespace."""
        if self.next_line is None:
            return None
        return self.next_line.rstrip()

    def get_preceding_line(self, relative_index=-1):
        """Retrieve the line at an line number at the given relative index, if one exists.

        Returns None if there is no line there.
        """
        if relative_index >= 0:
            raise RuntimeError(
                "relativeIndex must be negative, to retrieve a preceding line."
            )
        if relative_index + self.line_number <= 0:
            # There is no line at this index
            return None
        return self._lines[self.line_number + relative_index - 1]

    def get_preceding_lines(self, num):
        """Get *up to* the preceding num lines.

        Fewer may be returned if the requested number aren't available.
        """
        return self._lines[-(num + 1) : -1]

    def process_line(self, line_num, line):
        """Process a line supplied by LinewiseFileProcessor.

        Must implement in your subclass.
        """
        raise NotImplementedError

    def _process_file_handle(self, file_handle: TextIO) -> None:
        # These are so we can process one line earlier than we're actually iterating thru.
        processing_line_num = None
        processing_line = None

        def do_process_line():
            if processing_line is not None:
                assert processing_line_num is not None
                self._line_num = processing_line_num
                self._line = processing_line
                self._lines.append(processing_line)
                self.process_line(processing_line_num, processing_line)
            else:
                self._line = ""

        for line_num, line in enumerate(file_handle, 1):
            self._next_line = line
            do_process_line()
            processing_line_num = line_num
            processing_line = line

        # Finally process the left-over line
        self._next_line = None
        do_process_line()

    def process_file(
        self, filename: Union[str, Path], file_handle: Optional[TextIO] = None
    ) -> None:
        """Call with a filename and optionally the file handle to read from (Main entry point)."""
        if isinstance(filename, str):
            filename = Path(filename).resolve()

        self._filename = filename

        if file_handle:
            self._process_file_handle(file_handle)
        else:
            with self._filename.open("r", encoding="utf-8") as fp:
                self._process_file_handle(fp)
