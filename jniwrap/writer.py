# Copyright 2020-2021, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
"""
Generate an initial draft of some wrapping headers/cpp files.

These are likely to be modified by hand, so re-generation of existing files
should be done only in a version-controlled location, and the changes
should be accepted selectively after clang-format is applied,
to avoid overwriting manual improvements that are infeasible or impractical to
represent concisely in the data format used here.
"""

from datetime import datetime
import json
import logging
from pathlib import Path, PurePath
from typing import Any, Dict, List, Optional, Set, TextIO, Tuple, Union, cast

from .class_data import ClassData
from .processor import SigExtractor
from .types import (
    Array,
    JObject,
    NamedMemberOptions,
    OptionsDict,
    PrimitiveType,
    VisitorBase,
    qualified_name_parts_to_qualified_cpp,
)


def _open_namespace(namespace: str) -> str:
    return f"namespace {namespace} {{"


def _close_namespace(namespace: str) -> str:
    return f"}} // namespace {namespace}"


def _begin_body(package_component_list: List[str]) -> str:
    return "\n".join(
        (
            _open_namespace("wrap"),
            _open_namespace(
                qualified_name_parts_to_qualified_cpp(package_component_list)
            ),
        )
    )


def _end_body(package_component_list: List[str]) -> str:
    return "\n".join(
        (
            _close_namespace(
                qualified_name_parts_to_qualified_cpp(package_component_list)
            ),
            _close_namespace("wrap"),
            "",
        )
    )


class _ClassWrappingVisitor(VisitorBase):
    def __init__(self, wrapped_classes: Set[str]) -> None:
        super().__init__()
        self.wrapped_classes = wrapped_classes

    def visit_type(self, t: Union[PrimitiveType, Array, JObject]) -> None:
        if t.qualified_name in self.wrapped_classes:
            t.has_wrapper = True


def _write_fwd_decls(fwd_decls: Set[Tuple[str, ...]], fp: TextIO) -> None:
    namespaces_and_classes: Tuple[Tuple[str, str], ...] = tuple(
        ("::".join(decl[:-1]), decl[-1]) for decl in fwd_decls
    )
    # Make list for each name
    namespaces: Dict[str, List[str]] = {
        namespace: [] for namespace, _ in namespaces_and_classes
    }
    for namespace, classname in namespaces_and_classes:
        namespaces[namespace].append(classname)

    for namespace in sorted(namespaces.keys()):
        fp.write("namespace %s {\n" % namespace)
        for classname in sorted(namespaces[namespace]):
            fp.write("class %s;\n" % classname)
        fp.write("} // namespace %s\n\n" % namespace)


class _IncludeFixer:
    """Transforms generated includes into "fixed" ones.

    Assumes that the unique part of an include is the filename stem,
    and that the extension and directory may differ between what's generated
    and what is actually desired.
    """

    def __init__(self) -> None:
        self.nondefault_header_ext: Optional[str] = None

        self._known_includes: Dict[str, str] = {}
        """Maps include file stems to the full include strings."""

        self._cache: Dict[str, str] = {}
        """Maps full include strings to corrected full include strings."""

    def add_known_include(self, include: str) -> None:
        """Record a known include filename to use when processing includes."""
        path = PurePath(include)
        self._known_includes[path.stem] = f'"{include}"'

    def process(self, include: str) -> str:
        """Transform a generated include string into a "fixed" one."""
        if include.startswith("<"):
            # Can't do better.
            return include
        # Do we have this cached?
        cached = self._cache.get(include)
        if cached:
            return cached

        fn = include[1:-1]
        path = PurePath(fn)

        # Does this match a known stem?
        known = self._known_includes.get(path.stem)
        if known:
            self._cache[include] = known
            return known

        # Change the extension?
        if self.nondefault_header_ext:
            before_ext, _, ext = fn.rpartition(".")
            if ext == "h":
                result = f'"{before_ext}.{self.nondefault_header_ext}"'
                self._cache[include] = result
                return result

        # no changes
        self._cache[include] = include
        return include


class _PackageOptions:
    def __init__(self, header_extension: str) -> None:
        self.directory: str = "wrap"
        self.header_extension: str = header_extension
        self.header_doc_lines = ["// Author: Rylie Pavlik <rylie.pavlik@collabora.com>"]
        self.impl_header_doc_lines = self.header_doc_lines[:] + [
            "// Inline implementations: do not include on its own!"
        ]
        self.impl_doc_lines = self.header_doc_lines[:]

    def parse_config(self, options):
        """Update our state based on options from JSON."""
        self.directory = options.get("directory", self.directory)
        self.header_doc_lines = options.get("headerDocLines", self.header_doc_lines)
        self.impl_header_doc_lines = options.get(
            "implHeaderDocLines", self.impl_header_doc_lines
        )
        self.impl_doc_lines = options.get("implDocLines", self.impl_doc_lines)


# pylint: disable=too-many-instance-attributes
class WrapperWriter:
    """Class that writes wrappers."""

    def __init__(
        self,
        data_filename: str,
        framework: Path,
        extra_classpath: Optional[List[str]] = None,
    ) -> None:
        """Constructor."""
        self._log = logging.getLogger(self.__module__)
        self.only_this_class_type = None
        self.only_this_package = None
        with open(data_filename, "r", encoding="utf-8") as fp:
            self.data: Dict[str, Any] = json.load(fp)
        classpath = [str(framework)]
        if extra_classpath:
            classpath.extend(extra_classpath)
        self.processor = SigExtractor(classpath)
        self.package_data: Dict[str, List[ClassData]] = {}

        self._package_options: Dict[str, _PackageOptions] = {}

        # Things changed by global options
        self._include_fixer = _IncludeFixer()
        self.external_include_path = ""
        self.external_include_prefixes = []
        self.header_extension = "h"

    def handle_arg(self, arg: str) -> None:
        """Process a command line argument."""
        if arg in self.data:
            # This is a package name we were given
            self._log.info("Will only process package %s", arg)
            self.only_this_package = arg
        else:
            self._log.info("Will only process class %s", arg)
            self.only_this_class_type = JObject.from_qualified_type_name(arg)
            self.only_this_package = self.only_this_class_type.dotted_package_name

    # pylint: disable=no-self-use
    def _write_copyright(self, fp: TextIO, doc_lines: List[str]) -> None:
        year = datetime.now().year

        fp.write(f"// Copyright 2020-{year}, Collabora, Ltd.\n")
        # This string is broken to avoid confusing REUSE
        fp.write("// SPDX" + "-License-Identifier: BSL-1.0\n")
        for line in doc_lines:
            fp.write(f"{line}\n")
        fp.write("\n")

    def _process_include(self, include: str) -> str:
        return self._include_fixer.process(include)

    def write_package_header(self, package: str, classes: List[ClassData]) -> None:
        """Write headers for package."""
        self._log.info("Writing header for package %s", package)
        assert classes
        package_components = classes[0].type.package
        # fp = sys.stdout

        includes = set()
        fwd_decls = set()
        for classdata in classes:
            if self._should_generate_for_class(classdata):
                includes.update(
                    classdata.get_declaration_required_includes(package_components)
                )
                fwd_decls.update(classdata.get_forward_declarations())

        # Fix up includes
        includes = [self._process_include(f) for f in sorted(includes)]

        options = self._package_options[package]

        with open(f"{options.directory}/{package}.{self.header_extension}", "w") as fp:
            self._write_copyright(fp, options.header_doc_lines)
            fp.write("#pragma once\n\n")
            for f in includes:
                fp.write(f"#include {f}\n")
            fp.write("\n")

            if fwd_decls:
                fp.write("namespace wrap {\n")
                _write_fwd_decls(fwd_decls, fp)
                fp.write("} // namespace wrap\n")

            fp.write("\n\n")
            fp.write(_begin_body(package_components))
            fp.write("\n")
            for classdata in classes:
                if not self._should_generate_for_class(classdata):
                    continue
                fp.write(classdata.get_decl(package_components))
                fp.write("\n")
            fp.write(_end_body(package_components))

            fp.write(f'#include "{package}.impl.{self.header_extension}"\n')
            fp.write("\n")

    def write_package_impl_header(self, package: str, classes: List[ClassData]) -> None:
        """Write implementation headers for package."""
        self._log.info("Writing impl header for package %s", package)
        assert classes
        package_components = classes[0].type.package
        # fp = sys.stdout

        includes = set()
        for classdata in classes:
            if self._should_generate_for_class(classdata):
                includes.update(
                    classdata.get_definition_required_includes(package_components)
                )

        # Fix up includes
        includes = [self._process_include(f) for f in sorted(includes)]

        options = self._package_options[package]

        with open(
            f"{options.directory}/{package}.impl.{self.header_extension}", "w"
        ) as fp:
            self._write_copyright(fp, options.impl_header_doc_lines)
            fp.write("#pragma once\n\n")
            for f in includes:
                fp.write(f"#include {f}\n")

            fp.write("\n\n")
            fp.write(_begin_body(package_components))
            fp.write("\n")
            for classdata in classes:
                if not self._should_generate_for_class(classdata):
                    continue
                fp.write(classdata.get_inline_implementations(package_components))
                fp.write("\n")
            fp.write(_end_body(package_components))
            fp.write("\n")

    def write_package_impl(self, package: str, classes: List[ClassData]) -> None:
        """Write implementations for package."""
        self._log.info("Writing impl cpp for package %s", package)
        assert classes
        package_components = classes[0].type.package

        options = self._package_options[package]

        with open(f"{options.directory}/{package}.cpp", "w") as fp:
            self._write_copyright(fp, options.impl_doc_lines)
            fp.write(f'#include "{package}.{self.header_extension}"\n\n')
            fp.write(_begin_body(package_components))
            fp.write("\n")
            for classdata in classes:
                if not self._should_generate_for_class(classdata):
                    continue
                fp.write(classdata.get_meta_constructor())
                fp.write("\n")
            fp.write(_end_body(package_components))
            fp.write("\n")

    def _should_generate_for_package(self, package: str) -> bool:
        if self.only_this_package and self.only_this_package != package:
            return False
        return True

    def _should_generate_for_class_name(self, class_name: str) -> bool:
        if (
            self.only_this_class_type
            and self.only_this_class_type.qualified_name != class_name
        ):
            return False
        return True

    def _should_generate_for_class(self, classdata: ClassData) -> bool:
        return self._should_generate_for_class_name(classdata.type.qualified_name)

    def handle_global_options(self, options: Dict[str, Any]):
        """Process the data file's global options object."""
        known_includes: Optional[List[str]] = options.get("knownIncludes")
        if known_includes:
            for elt in known_includes:
                self._include_fixer.add_known_include(elt)
        header_extension: Optional[str] = options.get("headerExtension")
        if header_extension:
            self._include_fixer.nondefault_header_ext = header_extension
            self.header_extension = header_extension

    def load_classes(self) -> None:
        """Load classes according to data file."""
        self._log.info("Loading classes")

        known_classes: Set[str] = set()

        global_options: Optional[Dict] = self.data.get("$globalOptions")
        if global_options:
            self.handle_global_options(global_options)

        for package, classes in self.data.items():
            self.package_data[package] = self._process_package(
                known_classes, package, classes
            )

        # Flatten
        all_classes = [x for pkg in self.package_data.values() for x in pkg]
        visitor = _ClassWrappingVisitor(known_classes)

        for classdata in all_classes:
            classdata.assign_decorations()
            classdata.apply_visitor(visitor)

    def _process_package(
        self, known_classes: Set[str], package: str, classes: Dict[str, Any]
    ) -> List[ClassData]:
        if package[0] in ("$", "#"):
            return []
        known_classes.update(
            "{}.{}".format(package, classname)
            for classname in classes
            if classname[0] not in ("$", "#")
        )
        classes_data: List[ClassData] = []
        generate_package = self._should_generate_for_package(package)
        self._package_options[package] = _PackageOptions(self.header_extension)
        for classname, value in classes.items():
            classdata = self._process_class(
                package, generate_package, classname, value=value
            )
            if classdata:
                classes_data.append(classdata)
        return classes_data

    # pylint: disable=too-many-branches
    def _process_class(
        self,
        package: str,
        generate_package: bool,
        classname: str,
        value: Union[Dict[str, Any], List[Union[str, Dict[str, Any]]]],
    ):
        if classname[0] == "#":
            return None
        if classname == "$packageOptions":
            self._package_options[package].parse_config(value)
            return None
        generate_class = generate_package and self._should_generate_for_class_name(
            classname
        )
        if generate_class:
            classdata = self.processor.extract_class(package, classname)
        else:
            classdata = self.processor.mock_class(package, classname)

        member_options: Optional[Dict[str, NamedMemberOptions]] = None
        if isinstance(value, list):
            # Old schema
            # First element of the member list can actually be an options dict.
            if value and isinstance(value[0], dict):
                classdata.apply_options(cast(OptionsDict, value[0]))
                member_list: List[str] = cast(List[str], value[1:])
            else:
                member_list: List[str] = cast(List[str], value[:])
            if generate_class:
                member_options = {
                    elt: NamedMemberOptions.make_for_old_schema(package, classname, elt)
                    for elt in member_list
                }

        elif isinstance(value, dict):
            # new schema - the value is an object, not an array
            class_options = value.get("classOptions")
            if class_options:
                classdata.apply_options(class_options)
            if generate_class:
                member_options = {
                    k: NamedMemberOptions.from_json_dict(package, classname, k, v)
                    for k, v in value["members"].items()
                }

        else:
            raise RuntimeError("Should not get here, bad config file!")

        if generate_class and member_options is not None:
            classdata.process_member_options(member_options)

        return classdata

    def output_code(self) -> None:
        """Write the code we've been told to."""
        for package, classes in self.package_data.items():
            if not classes:
                continue
            if not self._should_generate_for_package(package):
                continue
            self.write_package_header(package, classes)
            self.write_package_impl(package, classes)
            self.write_package_impl_header(package, classes)
