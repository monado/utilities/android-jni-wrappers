// Copyright 2020-2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Out-of-line implementations for partially-generated wrapper for the
 * `org.freedesktop.monado.ipc` Java package.
 * @author Rylie Pavlik <rylie.pavlik@collabora.com>
 * @ingroup ipc_android
 */

#include "org.freedesktop.monado.ipc.hpp"

namespace wrap {
namespace org::freedesktop::monado::ipc {
	Client::Meta::Meta(jni::jclass clazz)
	    : MetaBase(Client::getTypeName(), clazz), init(classRef().getMethod("<init>", "(J)V")),
	      blockingConnect(
	          classRef().getMethod("blockingConnect", "(Landroid/content/Context;Ljava/lang/String;)I")),
	      failed(classRef(), "failed"),
	      markAsDiscardedByNative(classRef().getMethod("markAsDiscardedByNative", "()V")),
	      monado(classRef(), "monado")
	{}
	IMonado::Meta::Meta()
	    : MetaBase(IMonado::getTypeName()),
	      passAppSurface(classRef().getMethod("passAppSurface", "(Landroid/view/Surface;)V"))
	{}
	MonadoImpl::Meta::Meta() : MetaBase(MonadoImpl::getTypeName()), context(classRef(), "context") {}
} // namespace org::freedesktop::monado::ipc
} // namespace wrap
