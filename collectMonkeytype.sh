#!/bin/sh
# Copyright 2021, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
set -e

rm -f monkeytype.sqlite3

python3 -m monkeytype run -m jniwrap.members
python3 -m monkeytype run -m jniwrap.types
python3 -m monkeytype run ./makeWrapper.py
