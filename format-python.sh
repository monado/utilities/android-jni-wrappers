#!/bin/sh
# Copyright 2021, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
set -e

python3 -m isort .
python3 -m black jniwrap/*.py makeWrapper.py
python3 -m pylint jniwrap makeWrapper.py
